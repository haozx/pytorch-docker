# pytorch in ubuntu 16.04 with cuda9 and cudnn7

## Tags
- `latest` pytorch installed via anaconda with python3.6 and popular data science packages [Dockerfile](https://bitbucket.org/haozx/pytorch-docker/src/anaconda/Dockerfile)
- `pip` pytorch installed via pip with python3.5 [Dockerfile](https://bitbucket.org/haozx/pytorch-docker/src/master/Dockerfile)
